'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send
app.get('/', (req, res) => {
    const balance = 1000;

    const { amount } = req.query || {}
    if (typeof amount === 'undefined') {
      throw new Error('amount must be defined in query string')
    }

    var { balance: newBalance, transfered } = transfer(balance, amount);

    if (newBalance !== undefined || transfered !== undefined) {
        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + newBalance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + newBalance);
    }
});

class Amount {
  constructor(amount) {
    let amt = Number(amount)

    if (Number.isNaN(amt)) {
      throw new Error('amount is not a number')
    }

    if (amt > Number.MAX_SAFE_INTEGER) {
      throw new Error('amount is not a safe integer')
    }

    this.amount = amt
  }
}

// Transfer amount service
var transfer = (balance, amount) => {
    console.log('balance', balance, 'amount', amount)

    const amt = new Amount(amount)

    var transfered = 0;
    if (amt.amount >= 0 && amt.amount <= balance) {
        balance = balance - amt.amount;
        transfered = transfered + amt.amount;
        console.log('final', { balance, transfered })
        return { balance, transfered };
    } else
        return {};
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
